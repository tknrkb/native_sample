#include <stdio.h>
#include <ruby.h>

VALUE world(void);

void Init_native_sample_native(){
  VALUE mNativeSample;
  VALUE cHello;

  mNativeSample = rb_define_module("NativeSample");
  cHello = rb_define_class_under(mNativeSample, "Hello", rb_cObject);
  rb_define_method(cHello, "world", world, 0);
}

VALUE world(void){
  return rb_str_new2("hello world");
}
